# Install required Ubuntu packages. These are available to all Cedar-14 dynos:
# https://devcenter.heroku.com/articles/cedar-ubuntu-packages
#
#   libpcre3-dev
#     -> enables use of regular expressions in nginx `location` definitions
#
#   libssl-dev
#     -> enables sending and receiving SSL requests
#
#   git
#     -> Needed to check out the ngx_mruby source
#
#   ruby-full
#   bison
#     -> Needed to build ngx_mruby
#
sudo apt-get update
sudo apt-get install -y libpcre3-dev libssl-dev git ruby-full bison
sudo mkdir /cache

# Used in ngx_mruby's build process
gem install rake

# Emulate Heroku by switching to /app directory when SSHing into VM.
echo "cd /app" >> /home/vagrant/.bashrc
