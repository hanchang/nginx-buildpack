# Heroku Buildpack for nginx

This buildpack allows you to run nginx on a Heroku dyno.

## Features

* Downloads nginx source
* Compiles nginx, using the SSL and PCRE packages [installed by default on Heroku](https://devcenter.heroku.com/articles/cedar-ubuntu-packages)
* Adds `nginx` to your app's PATH
* Additional files (mime.types, example configs, etc) are available at `$HOME/nginx` if you need them.

## Using this buildpack

Set the `BUILDPACK_URL` ENV variable to the URL of this repo. Since this repo is private, you must provide an OAuth token. Simply generate a new personal access token for your app on the InvestmentZen Bitbucket account. In this example, the token is `abc123`:

```ruby
heroku config:set BUILDPACK_URL=https://abc123:x-oauth-basic@bitbucket.com/investmentzen/nginx-buildpack.git
```

After adding the buildpack, `nginx` will be available on your path.  It is the responsibility of your app and it's `Procfile` to call nginx with a `nginx.conf` that you've generated.

 **You must generate nginx.conf yourself** because what port you're using depends on the `PORT` environment variable, which is different on every app.

## Development

### Vagrant
A Vagrant development environment has been set up, using the same version of Ubuntu running on Heroku's [Cedar-14](https://devcenter.heroku.com/articles/cedar) dynos. To set up a development environment for the first time, install [Vagrant](https://www.vagrantup.com/downloads.html) and [VirtualBox](https://www.virtualbox.org/wiki/Downloads), then run the following commands:

```
$ git clone git@bitbucket.com:investmentzen/nginx-buildpack.git
$ cd nginx-buildpack
$ vagrant up
```

This will download, boot, and configure an Ubuntu VM with the packages needed to compile nginx (which are pre-installed on the Cedar-14 stack). You can then remote into the VM with:
```
$ vagrant ssh
```

This will drop you into the `/app` folder in your VM, which is mapped to the repo's root folder on your host machine. Now that you're all set up, you can test the buildpack with:
```
bin/compile $HOME/build $HOME/cache
```

See the excellent [documentation](https://docs.vagrantup.com/v2/) for more on how to use Vagrant in development.

### OS X
Alternatively, you can build this in OS X by running the command:

```
BUILD_PREFIX=$PWD ./bin/compile $PWD/build $PWD/cache
```

If you try to build on OS X and have issues related to OpenSSL, make sure you've
install OpenSSL using Homebrew and then you could try the following command:
```
BUILD_PREFIX=$PWD NGINX_OPTIONS="--with-cc-opt=-I$(brew --prefix)/opt/openssl/include --with-ld-opt=-L$(brew --prefix)/opt/openssl/lib" bin/compile $PWD/build $PWD/cache
```
It's ugly, but it should hopefully sort your issues out. Basically, it's just
providing an extra option to the Nginx compilation which points it to the paths
where OpenSSL is located, resolving the errors.

## Credits

This buildpack is based on ryandotsmith's [nginx-buildpack](https://github.com/ryandotsmith/nginx-buildpack). It has since been adapted for use at InvestmentZen.
